import 'package:flutter/material.dart';
import 'package:frenapp/src/models/Task.dart';
import 'package:frenapp/src/models/afiliacion_model.dart';
import 'package:frenapp/src/models/contenidoFormacion_model.dart';
import 'package:frenapp/src/models/preguntas_model.dart';
import 'package:frenapp/src/pages/Checkout.dart';
import 'package:frenapp/src/pages/contenido_formacion_detalle.dart';
import 'package:frenapp/src/pages/detallePregunta.dart';
import 'package:frenapp/src/pages/detalle_tarea.dart';
import 'package:frenapp/src/pages/pago_cuota.dart';
import 'package:frenapp/src/pages/payment_method.dart';
import 'package:frenapp/src/pages/preguntas_frecuentes.dart';
import 'package:frenapp/src/pages/procesos_organizacionales.dart';
import 'package:frenapp/src/pages/recovery_password.dart';
import 'package:frenapp/src/pages/tareas.dart';
import 'package:frenapp/src/pages/tu_afiliacion.dart';
import 'package:frenapp/src/models/route_argument.dart';
import 'package:frenapp/src/pages/afiliacion_sindical.dart';
import 'package:frenapp/src/pages/asambleas.dart';
import 'package:frenapp/src/pages/asesorias_juridicas.dart';
import 'package:frenapp/src/pages/contenidos_formacion.dart';
import 'package:frenapp/src/pages/editar_perfil.dart';
import 'package:frenapp/src/pages/formulario_afiliacion.dart';
import 'package:frenapp/src/pages/inicio.dart';
import 'package:frenapp/src/pages/login_page.dart';
import 'package:frenapp/src/pages/registro_page.dart';
import 'package:frenapp/src/pages/splashScreen_page.dart';
import 'package:frenapp/src/pages/unirse_meeting.dart';

class RouteGenerator {
  // ignore: missing_return
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/Splash':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/Login':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case '/recovery-password':
        return MaterialPageRoute(builder: (_) => RecoveryPasswordPage());
      case '/Registro':
        return MaterialPageRoute(builder: (_) => RegistroPage());
      case '/Home':
        return MaterialPageRoute(builder: (_) => HomePage());
      case '/PreguntasFrecuentes':
        return MaterialPageRoute(builder: (_) => PreguntasFrecuentes());
      case '/FormularioAfiliacion':
        return MaterialPageRoute(builder: (_) => FormularioAfliacionPage());
      case '/AfiliacionSindical':
        return MaterialPageRoute(
            builder: (_) =>
                AfiliacionSindicalPage(registroAfiliacion: args as Afiliacion));
      case '/PreguntaDetalle':
        return MaterialPageRoute(
            builder: (_) => PreguntaDetalle(pregunta: args as Preguntas));
      case '/ContenidoFormacionDetalle':
        return MaterialPageRoute(
            builder: (_) =>
                DetalleContenidoFormacion(contenido: args as Contenidos));
      case '/AsesoriasJuridicas':
        return MaterialPageRoute(builder: (_) => AsesoriasJuridicas());
      case '/ContenidoFormacion':
        return MaterialPageRoute(builder: (_) => ContenidosFormacion());
      case '/Asamblea':
        return MaterialPageRoute(builder: (_) => Asambleas());
      case '/UnirseReunion':
        return MaterialPageRoute(
            builder: (_) =>
                UnirseMeetingWidget(routeArgument: args as RouteArgument));
      case '/EditarPerfil':
        return MaterialPageRoute(builder: (_) => EditarPefil());
      case '/TuSindicato':
        return MaterialPageRoute(builder: (_) => TuSindicato());
      case '/ProcesosOrganizacionales':
        return MaterialPageRoute(builder: (_) => ProcesosOrganizacionales());
      case '/PagoCuotaSindical':
        return MaterialPageRoute(builder: (_) => PagoCuotaPage());
      case '/Tareas':
        return MaterialPageRoute(builder: (_) => TareasWidget());
      case '/PaymentMethod':
        return MaterialPageRoute(builder: (_) => PaymentMethodPage());
      case '/Checkout':
        return MaterialPageRoute(builder: (_) => CheckOutPage());
      case '/TareaDetalle':
        return MaterialPageRoute(
            builder: (_) => DetalleTarea(detalleTarea: args as Tareas));
    }
  }
}
