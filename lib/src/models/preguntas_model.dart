class FaqsFrecuentes {
  int registros;
  List<Preguntas> preguntas;

  FaqsFrecuentes({this.registros, this.preguntas});

  FaqsFrecuentes.fromJson(Map<String, dynamic> json) {
    registros = json['registros'];
    if (json['preguntas'] != null) {
      preguntas = new List<Preguntas>();
      json['preguntas'].forEach((v) {
        preguntas.add(new Preguntas.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['registros'] = this.registros;
    if (this.preguntas != null) {
      data['preguntas'] = this.preguntas.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Preguntas {
  String sId;
  String titulo;
  String detalles;
  int iV;

  Preguntas({this.sId, this.titulo, this.detalles, this.iV});

  Preguntas.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    titulo = json['titulo'];
    detalles = json['detalles'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['titulo'] = this.titulo;
    data['detalles'] = this.detalles;
    data['__v'] = this.iV;
    return data;
  }
}
