class ContenidosFormacionModel {
  int registros;
  List<Contenidos> contenidos;

  ContenidosFormacionModel({this.registros, this.contenidos});

  ContenidosFormacionModel.fromJson(Map<String, dynamic> json) {
    registros = json['registros'];
    if (json['contenidos'] != null) {
      contenidos = new List<Contenidos>();
      json['contenidos'].forEach((v) {
        contenidos.add(new Contenidos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['registros'] = this.registros;
    if (this.contenidos != null) {
      data['contenidos'] = this.contenidos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Contenidos {
  String sId;
  String titulo;
  String subtitulo;
  String detalle;
  String video;
  int iV;

  Contenidos(
      {this.sId,
      this.titulo,
      this.subtitulo,
      this.detalle,
      this.video,
      this.iV});

  Contenidos.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    titulo = json['titulo'];
    subtitulo = json['subtitulo'];
    detalle = json['detalle'];
    video = json['video'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['titulo'] = this.titulo;
    data['subtitulo'] = this.subtitulo;
    data['detalle'] = this.detalle;
    data['video'] = this.video;
    data['__v'] = this.iV;
    return data;
  }
}
