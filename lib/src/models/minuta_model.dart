class Minuta {
  String usuario;
  String actaNo;
  String fecha;
  String hora;
  String tipoAsamblea;
  List<String> ordenDia;
  String resumen;

  Minuta(
      {this.usuario,
      this.actaNo,
      this.fecha,
      this.hora,
      this.tipoAsamblea,
      this.ordenDia,
      this.resumen});

  Minuta.fromJson(Map<String, dynamic> json) {
    usuario = json['usuario'];
    actaNo = json['actaNo'];
    fecha = json['fecha'];
    hora = json['hora'];
    tipoAsamblea = json['tipoAsamblea'];
    ordenDia = json['ordenDia'].cast<String>();
    resumen = json['resumen'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['usuario'] = this.usuario;
    data['actaNo'] = this.actaNo;
    data['fecha'] = this.fecha;
    data['hora'] = this.hora;
    data['tipoAsamblea'] = this.tipoAsamblea;
    data['ordenDia'] = this.ordenDia;
    data['resumen'] = this.resumen;
    return data;
  }
}
