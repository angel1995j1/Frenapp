class TuSindicato {
  String sId;
  String texto;
  String createdAt;
  String updatedAt;
  int iV;

  TuSindicato({this.sId, this.texto, this.createdAt, this.updatedAt, this.iV});

  TuSindicato.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    texto = json['texto'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['texto'] = this.texto;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}
