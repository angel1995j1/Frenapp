class Asesoria {
  String usuario;
  String descripcion;
  String file;
  String opciones;

  Asesoria({this.usuario, this.descripcion, this.file});

  Asesoria.fromJson(Map<String, dynamic> json) {
    usuario = json['usuario'];
    descripcion = json['descripcion'];
    file = json['file'];
    opciones = json['opciones'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['usuario'] = this.usuario;
    data['descripcion'] = this.descripcion;
    data['file'] = this.file;
    data['opciones'] = this.opciones;
    return data;
  }
}
