import 'package:flutter/widgets.dart';

class PaymentMethod {
  String id;
  String name;
  String description;
  String logo;
  String route;
  bool isDefault;
  bool selected;

  PaymentMethod(this.id, this.name, this.description, this.route, this.logo,
      {this.isDefault = false, this.selected = false});
}

class PaymentMethodList {
  List<PaymentMethod> _paymentsList;

  PaymentMethodList(BuildContext _context) {
    this._paymentsList = [
      new PaymentMethod(
          "mastercard",
          'Pago con tarjeta de crédito/débito',
          'Click para pagar con tarjeta',
          "/Checkout",
          "assets/img/mastercard.png",
          isDefault: true),
      new PaymentMethod(
          "in_app_purchase",
          'Pago con su wallet',
          'Click para pagar con wallet',
          "/PagoCuotaSindical",
          "assets/img/wallet.png"),
    ];
  }

  List<PaymentMethod> get paymentsList => _paymentsList;
}
