class ResponseRegistro {
  String rol;
  bool estado;
  bool facebook;
  String sId;
  String email;
  String password;
  String nombre;
  String telefono;
  String img;
  int iV;

  ResponseRegistro(
      {this.rol,
      this.estado,
      this.facebook,
      this.sId,
      this.email,
      this.password,
      this.nombre,
      this.telefono,
      this.img,
      this.iV});

  ResponseRegistro.fromJson(Map<String, dynamic> json) {
    rol = json['rol'];
    estado = json['estado'];
    facebook = json['facebook'];
    sId = json['_id'];
    email = json['email'];
    password = json['password'];
    nombre = json['nombre'];
    telefono = json['telefono'];
    img = json['img'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rol'] = this.rol;
    data['estado'] = this.estado;
    data['facebook'] = this.facebook;
    data['_id'] = this.sId;
    data['email'] = this.email;
    data['password'] = this.password;
    data['nombre'] = this.nombre;
    data['telefono'] = this.telefono;
    data['img'] = this.img;
    data['__v'] = this.iV;
    return data;
  }
}
