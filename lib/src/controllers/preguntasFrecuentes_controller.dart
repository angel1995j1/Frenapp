import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:frenapp/src/models/preguntas_model.dart';
import '../api/preguntasfrecuentes_repository.dart' as repo;

class PreguntasFrecuentesController extends ControllerMVC {
  List<FaqsFrecuentes> faqs = <FaqsFrecuentes>[];
  List<Preguntas> preguntas = <Preguntas>[];

  GlobalKey<ScaffoldState> scaffoldKey;

  PreguntasFrecuentesController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    obtenerPreguntas();
  }

  void obtenerPreguntas() async {
    repo.getPreguntasFrecuentes().then((value) => {
          faqs.add(value),
          for (var i = 0; i < value.preguntas.length; i++)
            {
              setState(() {
                preguntas.add(value.preguntas[i]);
              })
            },
          print('preguntas ${preguntas.length}')
        });
  }

  Future<void> onRefresh() async {
    faqs = [];
    obtenerPreguntas();
  }
}
