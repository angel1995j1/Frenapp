import 'package:flutter/material.dart';
import 'package:frenapp/src/elements/CircularLoadingWidget.dart';

class PagoCuotaPage extends StatefulWidget {
  PagoCuotaPage({Key key}) : super(key: key);

  @override
  _PagoCuotaPageState createState() => _PagoCuotaPageState();
}

class _PagoCuotaPageState extends State<PagoCuotaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pago de cuota sindical'),
      ),
      body: SingleChildScrollView(
        child: CircularLoadingWidget(),
      ),
    );
  }
}
