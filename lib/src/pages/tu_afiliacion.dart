import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:frenapp/src/controllers/usuario_controller.dart';
import 'package:frenapp/src/utils/helpers.dart';
import 'package:frenapp/src/api/user_repository.dart';
import 'package:frenapp/src/elements/DrawerWidget.dart';
import 'package:frenapp/src/elements/PermisosDenegadosWidget.dart';

class TuSindicato extends StatefulWidget {
  TuSindicato({Key key}) : super(key: key);

  @override
  _TuSindicatoState createState() => _TuSindicatoState();
}

class _TuSindicatoState extends StateMVC<TuSindicato> {
  UserController _con;

  _TuSindicatoState() : super(UserController()) {
    _con = controller;
  }

  dynamic htmlSindicato;
  final Uri _emailLaunchUri = Uri(
      scheme: 'mailto',
      path: 'frenappecuador@gmail.com',
      queryParameters: {'subject': 'Hola!'});
  _launchURL() async {
    const url =
        'https://frena.fra1.digitaloceanspaces.com/ESTATUTOS%20FRENAPP.pdf';
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  @override
  void initState() {
    super.initState();
    _con.getInfoSindicato();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: DrawerWidget(),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: Builder(
            builder: (BuildContext context) {
              return new IconButton(
                icon:
                    Icon(Icons.arrow_back, color: Theme.of(context).hintColor),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              );
            },
          ),
        ),
        body: !perfilUsuario.value.afiliado
            ? PermisosDenegados()
            : SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                          child: Padding(
                        padding: const EdgeInsets.only(right: 50),
                        child: Text(
                          "Conoce tu Sindicato",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      )),
                    ),
                    Divider(
                      indent: 190,
                      height: 10,
                      thickness: 2,
                      color: Colors.black,
                    ),
                    SizedBox(height: 10),
                    Center(
                        child: Text('Tu Sindicato',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold))),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Helper.applyHtml(context, _con.infoSindicato.texto),
                          const SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            onTap: () {
                              _launchURL();
                            },
                            child: const Text(
                              'Ver estatutos de la organización sindical Frente de los trabajadores de plataformas digitales del Ecuador (FRENAPP)',
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                  fontSize: 16),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(height: 10),
                    Container(
                        height: 180,
                        width: 290,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          image: DecorationImage(
                            image: AssetImage('assets/img/tusindicato.png'),
                            fit: BoxFit.fill,
                          ),
                          shape: BoxShape.rectangle,
                        )),
                    SizedBox(height: 25),
                    Padding(
                      padding: const EdgeInsets.only(right: 60),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            icon: Icon(FontAwesomeIcons.twitter,
                                size: 25, color: Colors.blue),
                            onPressed: () {
                              launch('https://twitter.com/FrenAppEcuador');
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.facebookSquare,
                                size: 25, color: Colors.blue[900]),
                            onPressed: () {
                              launch('https://web.facebook.com/FrenApp');
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.google,
                                size: 25, color: Color(0xFFE24134)),
                            onPressed: () {
                              launch(_emailLaunchUri.toString());
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.instagram,
                                size: 25, color: Color(0xFF7B46B8)),
                            onPressed: () {
                              launch(
                                  'https://www.instagram.com/frenappecuador?r=nametag');
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.youtube,
                                size: 25, color: Colors.red[700]),
                            onPressed: () {
                              launch(
                                  'https://www.youtube.com/channel/UCEK8jsNMwjEb0NoI1W_X32A');
                            },
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.tiktok,
                                size: 25, color: Colors.black),
                            onPressed: () {
                              launch('https://vm.tiktok.com/ZM8Xg8pbG/');
                            },
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 40),
                  ],
                ),
              ));
  }
}
